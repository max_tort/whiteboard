import express, { Express } from 'express'
import { createServer, Server as HTTPServer } from 'http'
import { SocketGateway } from './SocketGateway'
import { WhiteboardService } from './WhiteboardService'
import { NoteDto } from '../../shared/NoteDto'
import cors from 'cors'
import { nanoid } from 'nanoid'

const INITIAL_STATE: NoteDto[] = [
  {
    name: nanoid(),
    text: 'You can drag notes around. When you do that, trash zone appear. If you drop you note there, it will disappear. ✌️😊',
    x: 10,
    y: 10
  },
  {
    name: nanoid(),
    text: 'Click on free space to create a note and start typing! 🥳🥳',
    x: 220,
    y: 10
  },
  {
    name: nanoid(),
    text: "Press esc if you're finished writing your note. 💁",
    x: 10,
    y: 220
  },
  {
    name: nanoid(),
    text: "You can notice that when someone else is editing the note, you can't edit it 🙈",
    x: 220,
    y: 220
  },
]

export class Application  {
  private http: HTTPServer
  app: Express
  socketGateway: SocketGateway

  constructor() {
    this.app = express()
    this._init()
    this.http = createServer(this.app)
    const whiteboard = new WhiteboardService(INITIAL_STATE)
    this.socketGateway = new SocketGateway(this.http, whiteboard)
  }

  private _init(): void {
    this.app.use(cors({
      origin: '*'
    }))
  }

  listen(): void {
    this.http.listen(3000)
  }
}