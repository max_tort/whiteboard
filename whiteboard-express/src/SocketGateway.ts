import { Server as SocketServer, Socket } from "socket.io";
import { Server as HTTPServer } from 'http'
import { WhiteboardService } from "./WhiteboardService";
import { NoteDto } from "../../shared/NoteDto";
import { GatewayEvents } from '../../shared/utilities'

export class SocketGateway {
  private io: SocketServer
  private whiteboard: WhiteboardService
  
  constructor(httpServer: HTTPServer, whiteboard: WhiteboardService) {
    this.io = new SocketServer(httpServer, {
      cors: {
        origin: '*'
      }
    })
    this.whiteboard = whiteboard
    this._configureEvents()
  }

  private _configureEvents(): void {
    this.io
      .on('connection', socket => this._onConnection(socket))
  }

  private _onConnection(socket: Socket): void {
    socket.emit('connection', this.whiteboard.getNotes())
    socket
      .on(GatewayEvents.onNoteChange, (payload: NoteDto) => this._onNoteChange(socket, payload))
      .on(GatewayEvents.onNoteCreate, (payload: NoteDto) => this._onNoteCreate(socket, payload))
      .on(GatewayEvents.onNoteLock, (name: string) => this._onNoteLock(socket, name))
      .on(GatewayEvents.onNoteUnlock, (name: string) => this._onNoteUnlock(socket, name))
      .on(GatewayEvents.onNoteDelete, (name: string) => this._onNoteDelete(socket, name))
  }

  private _onNoteChange(socket: Socket, payload: NoteDto): void {
    this.whiteboard.editNote(payload)
    socket.broadcast.emit(GatewayEvents.onNoteChange, payload)
  }

  private _onNoteCreate(socket: Socket, payload: NoteDto): void {
    this.whiteboard.appendNote(payload)
    socket.broadcast.emit(GatewayEvents.onNoteCreate, payload)
  }

  private _onNoteLock(socket: Socket, name: string): void {
    socket.broadcast.emit(GatewayEvents.onNoteLock, name)
  }

  private _onNoteUnlock(socket: Socket, name: string): void {
    socket.broadcast.emit(GatewayEvents.onNoteUnlock, name)
  }

  private _onNoteDelete(socket: Socket, name: string): void {
    this.whiteboard.delete(name)
    socket.broadcast.emit(GatewayEvents.onNoteDelete, name)
  }
}