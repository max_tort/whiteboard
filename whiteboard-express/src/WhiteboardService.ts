import { NoteDto } from '../../shared/NoteDto'

export class WhiteboardService {
  private notes: NoteDto[]

  constructor(initialState: NoteDto[]) {
    this.notes = initialState
  }

  getNotes(): NoteDto[] {
    return this.notes
  }

  editNote(note: NoteDto) {
    const foundNoteIndex = this.notes.findIndex((element: NoteDto) => note.name === element.name)
    if (foundNoteIndex !== -1)
      this.notes[foundNoteIndex] = note
  }

  appendNote(note: NoteDto) {
    this.notes.push(note)
  }

  delete(name: string) {
    const index = this.notes.findIndex((e) => e.name === name)
    if (index != -1) this.notes.splice(index, 1)
  }
}