import typescript from '@rollup/plugin-typescript'
import resolve from '@rollup/plugin-node-resolve'
import { babel } from '@rollup/plugin-babel'
import serve from 'rollup-plugin-serve'
import livereload from 'rollup-plugin-livereload'
import commonjs from '@rollup/plugin-commonjs'
import { uglify } from 'rollup-plugin-uglify'
import strip from '@rollup/plugin-strip'

const production = !process.env.ROLLUP_WATCH;

export default {
  input: 'src/main.ts',
  output: {
    dir: 'public/build',
    format: 'umd',
    sourcemap: true
  },
  watch: {
    include: ['src/**'],
    exclude: ['node_modules/**']
  },
  plugins: [
    commonjs(),
    resolve({ preferBuiltins: false, browser: true }),
    // strip(),
    typescript(),
    babel({
      babelHelpers: 'bundled',
    }),
    !production && serve({
      port: 3001,
      contentBase: 'public'
    }),
    !production && livereload(),
    production && uglify()
  ]
}