import io, { Socket } from "socket.io-client";
import { NoteDto } from '../../shared/NoteDto'
import { GatewayEvents } from '../../shared/utilities'

const API_URL = 'https://whiteboard-899y5.ondigitalocean.app'

export class APIManager {
  private socketioClient: Socket

  onConnect?: (payload: NoteDto[]) => void
  onNoteChange?: (payload: NoteDto) => void
  onNoteCreate?: (payload: NoteDto) => void
  onNoteLock?: (name: string) => void
  onNoteUnlock?: (name: string) => void
  onNoteDelete?: (name: string) => void

  constructor() {
    this.socketioClient = io(API_URL, {
      autoConnect: false
    })
    this._attachListeners()
  }

  private _attachListeners(): void {
    this.socketioClient.on('connection', (payload: NoteDto[]) => {
      if (this.onConnect) this.onConnect(payload)
    })

    this.socketioClient.on(GatewayEvents.onNoteCreate, (payload: NoteDto) => {
      if (this.onNoteCreate) this.onNoteCreate(payload)
    })

    this.socketioClient.on(GatewayEvents.onNoteChange, (payload: NoteDto) => {
      if (this.onNoteChange) this.onNoteChange(payload)
    })

    this.socketioClient.on(GatewayEvents.onNoteDelete, (name: string) => {
      if (this.onNoteDelete) this.onNoteDelete(name)
    })

    this.socketioClient.on(GatewayEvents.onNoteLock, (name: string) => {
      if (this.onNoteLock) this.onNoteLock(name)
    })

    this.socketioClient.on(GatewayEvents.onNoteUnlock, (name: string) => {
      if (this.onNoteUnlock) this.onNoteUnlock(name)
    })
  }

  pushNoteChanges(payload: NoteDto): void {
    this.socketioClient.emit(GatewayEvents.onNoteChange, payload)
  }
  pushNoteCreate(payload: NoteDto): void {
    this.socketioClient.emit(GatewayEvents.onNoteCreate, payload)
  }
  pushNoteDelete(name: string): void {
    this.socketioClient.emit(GatewayEvents.onNoteDelete, name)
  }
  pushNoteLock(name: string): void {
    this.socketioClient.emit(GatewayEvents.onNoteLock, name)
  }
  pushNoteUnlock(name: string): void {
    this.socketioClient.emit(GatewayEvents.onNoteUnlock, name)
  }

  connect(): void {
    this.socketioClient.connect()
  }
}