import anime from 'animejs';
import * as PIXI from 'pixi.js'
import { NoteDto } from '../../shared/NoteDto';

const DEFAULT_SIZE = { width: 200, height: 200 }
const DEFAULT_PADDING = 16
const DEFAULT_TEXT = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo impedit voluptatibus corrupti id, dignissimos ea ab exercitationem odit.'
const DEFAULT_DRAG_DISTANCE = 2
const DEFAULT_LINE_COUNT = 7
const DEFAULT_TEXT_STYLE = {
  fontFamily: 'Arial, Helvetica, sans-serif',
  fontSize: 16,
  wordWrap: true,
  breakWords: true,
  lineHeight: 24
}

// TODO
// 1. don't allow change in position for more than the bounds allow.
// 2. try to fit every note inside the stage
// 3. write toDTO and back
// 4. optimize for mobile and low-end devices

export class Note extends PIXI.Container {
  private _shadowGraphics: PIXI.Graphics
  private _noteGraphics: PIXI.Graphics
  private _lineGraphics: PIXI.Graphics[]
  private _text: PIXI.Text
  private _shadowInput: HTMLTextAreaElement

  // fields for interaction. E.g. DragStart, DragEnd, DragMove e.t.c.
  private _dragging: boolean = false
  private _data: PIXI.InteractionData | null = null
  private _localCoordinates: PIXI.Point = new PIXI.Point()

  constructor(x: number, y: number, name: string, text: string = DEFAULT_TEXT, width: number = DEFAULT_SIZE.width, height: number = DEFAULT_SIZE.height, padding: number = DEFAULT_PADDING) {
    super();
    this.name = name;
    [this.x, this.y] = [x, y]
    this._shadowGraphics = this._getShadowGraphics(width, height)
    this._noteGraphics = this._getNoteGraphics(width, height)
    this._lineGraphics = this._getLineGraphics(width, height, padding)
    this._text = this._getText(width, height, padding, text)
    this.addChild(this._shadowGraphics, this._noteGraphics, ...this._lineGraphics, this._text)
    this._shadowInput = this._getShadowTextArea()
    this._updateShadowInputPosition()
    document.body.appendChild(this._shadowInput)
    this._setUpInteractions()
    this._appear()
  }

  private _appear(): void {
    this.alpha = 0
    anime({ targets: this, alpha: 1, easing: 'easeOutCirc', duration: 200})
  }

  private _setUpInteractions(): void {
    this.interactive = true
    this
      .on('pointerover', this._onPointerOver)
      .on('pointerout', this._onPointerOut)
      .on('pointerdown', this._onPointerDown)
      .on('pointerup', this._onPointerUp)
      .on('pointerupoutside', this._onPointerUpOutside)
      .on('pointermove', this._onPointerMove)
    this._shadowInput.oninput = this._onInput.bind(this)
    this._shadowInput.onblur = this._onBlur.bind(this)
    this._shadowInput.onkeyup = this._onKeyPress.bind(this)
  }

  // gets fired when user inputs something
  private _onInput(event: Event): void {
    this._text.text = this._shadowInput.value
    this.emit('input', event)
  }

  // gets fired when user blurs from input
  private _onBlur(event: Event): void {
    this._shadowInput.style.display = 'none'
    this.emit('blur', event)
  }

  private _onKeyPress(event: KeyboardEvent): void {
    if (event.key === 'Escape') {
      this._shadowInput.blur()
      this._shadowInput.style.display = 'none'
    }
  }

  // gets fired when cursor is over the container
  private _onPointerOver(): void {
    if (this._dragging) return
    // this._moveTop()
    this.state = 'hover'
  }

  // gets fired when the cursor left the container
  private _onPointerOut(): void {
    if (this._dragging) return
    this.state = 'default'
  }

  // gets fired when user starts holding the pointer button over the container
  private _onPointerDown(event: PIXI.InteractionEvent): void {
    this._moveTop()
    this._data = event.data
    this._localCoordinates = this._data.getLocalPosition(this)
    this.cursor = 'grab'
  }

  // gets fired when user stops holding the pointer button over the container
  private _onPointerUp(event: PIXI.InteractionEvent): void {
    this.state = 'hover'
    this._data = null
    if (this._dragging) {
      this.emit('dragend', event)
      this._dragging = false
      this._updateShadowInputPosition()
    }
    else {
      this.focus()
      this.emit('focus', event)
    }
  }

  // gets fired when user stops holding the pointer button outside of the container
  private _onPointerUpOutside(event: PIXI.InteractionEvent): void {
    this.state = 'default'
    this._data = null
    if (this._dragging) {
      this.emit('dragend', event)
      this._dragging = false
      this._updateShadowInputPosition()
    }
  }

  // gets fired when user moves the cursor around
  private _onPointerMove(event: PIXI.InteractionEvent): void {

    // in case no interaction data was initialized
    if (!this._data) return

    if (this._dragging) {
      const parentCoordinates = this._data.getLocalPosition(this.parent)
      this.x = parentCoordinates.x - this._localCoordinates.x
      this.y = parentCoordinates.y - this._localCoordinates.y
      this.emit('dragmove', event)
    }
    else {
      const currentLocalCoordinates = this._data.getLocalPosition(this)
      // we have to separate click event from drag event by using minimum travel distance before drag starts
      const distance = Math.abs(this._localCoordinates.x - currentLocalCoordinates.x) + Math.abs(this._localCoordinates.y - currentLocalCoordinates.y)
      if (distance > DEFAULT_DRAG_DISTANCE) {
        this._dragging = true
        this.state = 'drag'
        // emitting custom event when drag actually starts
        this.emit('dragstart', event)
      }
    }
  }
  
  // initialization of the backdrop shadow
  private _getShadowGraphics(width: number, height: number): PIXI.Graphics {
    return new PIXI.Graphics().beginFill(0x000000).drawRect(8, 8, width, height).endFill()
  }

  // initialization of the note box
  private _getNoteGraphics(width: number, height: number): PIXI.Graphics {
    return new PIXI.Graphics().lineStyle(2, 0x000000, 1).beginFill(0xF7F3F2).drawRect(0, 0, width, height).endFill()
  }

  // initialization of the lines
  private _getLineGraphics(width: number, height: number, padding: number): PIXI.Graphics[] {
    const graphics: PIXI.Graphics[] = []
    for (let i = 0; i < DEFAULT_LINE_COUNT; i++) {
      const endY: number = padding + DEFAULT_TEXT_STYLE.fontSize + (i * DEFAULT_TEXT_STYLE.lineHeight!) + 2
      graphics.push(
        new PIXI.Graphics()
          .lineStyle(2, 0xe5e0df, 1)
          .moveTo(padding, endY)
          .lineTo(width - padding, endY)
      )
    }
    return graphics
  }

  // initialization of the text
  private _getText(width: number, height: number, padding: number, text: string): PIXI.Text {
    const textStyle = new PIXI.TextStyle({
      ...DEFAULT_TEXT_STYLE,
      fontWeight: 'bold',
      wordWrapWidth: width - 2 * padding
    })
    const textGraphics = new PIXI.Text(text, textStyle);
    textGraphics.x = textGraphics.y = padding
    return textGraphics
  }

  private _getShadowTextArea(): HTMLTextAreaElement {
    const textarea = document.createElement('textarea')
    textarea.setAttribute('rows', '7')
    textarea.className = 'shadow-textarea'
    textarea.style.height = this._text.style.lineHeight! * DEFAULT_LINE_COUNT + 'px'
    textarea.style.width = this._text.style.wordWrapWidth + 'px'
    textarea.style.display = 'none'
    textarea.value = this._text.text
    return textarea
  }

  private _updateShadowInputPosition(): void {
    const textPosition = this._text.getGlobalPosition()
    this._shadowInput.style.top = textPosition.y + 'px'
    this._shadowInput.style.left = textPosition.x + 'px'
  }

  private _cleanup(): void {
    document.body.removeChild(this._shadowInput)
    this.removeAllListeners()
    const parent = this.parent
    parent.removeChild(this)
    // TODO
    // cleanup the rest
  }

  focus(): void {
    this._shadowInput.style.display = 'block'
    this._shadowInput.focus()
    this.emit('focus')
  }

  dissapear(): void {
    anime({
      targets: this,
      width: this.width / 2,
      height: this.height / 2,
      alpha: 0
    })
    this.emit('delete', this.name)
    this._cleanup()
  }

  // five different animated states of the Note
  private set state(state: 'default' | 'hover' | 'drag' | 'active' | 'hidden') {
    switch (state) {
      case 'hover':
        this.cursor = 'pointer'
        anime({ targets: this._shadowGraphics, x: 8, y: 8, alpha: 0.9, easing: 'easeOutCirc', duration: 300 })
        anime({ targets: this, rotation: 0, easing: 'easeOutCirc', duration: 300 })
        break
      case 'drag':
        this.cursor = 'grabbing'
        anime({ targets: this._shadowGraphics, x: 16, y: 16, alpha: 0.8, easing: 'easeOutCirc', duration: 300 })
        anime({ targets: this, rotation: 0.05, easing: 'easeOutCirc', duration: 300 })
        break
      case 'default':
      default:
        this.cursor = 'pointer'
        anime({ targets: this._shadowGraphics, x: 0, y: 0, easing: 'easeOutCirc', duration: 300 })
        anime({ targets: this, rotation: 0, easing: 'easeOutCirc', duration: 300 })
        break
    }
  }

  // lock note interactivity in case other user using it
  disable(): void {
    this.interactive = false
    this._noteGraphics.alpha = 0.8
    // disable text input
  }

  // enables interactivity
  enable(): void {
    this.interactive = true
    this._noteGraphics.alpha = 1
  }

  toDto(): NoteDto {
    return {
      name: this.name,
      x: this.x,
      y: this.y,
      text: this._text.text
    }
  }

  update({name, text, x, y}: NoteDto): void {
    this.x = x
    this.y = y
    this._text.text = text
    this._shadowInput.value = text
  }
  
  // moves this to the top of the parent's child list
  private _moveTop(): void {
    const parent = this.parent
    parent.removeChild(this)
    parent.addChild(this)
  }
}