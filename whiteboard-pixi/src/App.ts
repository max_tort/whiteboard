import { nanoid } from 'nanoid'
import * as PIXI from 'pixi.js'
import { NoteDto } from '../../shared/NoteDto'
import { APIManager } from './APIManager'
import { Note } from './Note'
import { Trash } from './Trash'
import { debounce, throttle } from './utility'

const DEFAULT_CURSOR = "url('/cursors/add.svg'), default"
const DRAG_CURSOR = "url(/cursors/cursor-drag-clicked.png), grabbing"
const POINTER_CURSOR = "url(/cursors/cursor-pointer.png), pointer"
const POINTER_DOWN_CURSOR = "url(/cursors/cursor-pointer-clicked.png), grab"

// TODO:
// 1. resize container on window resize. Implement method: onWindowResize(window: Window) e.t.c.
// 2. Optimize

export class Application extends PIXI.Application {

  private _trash?: Trash
  private _apiManager?: APIManager
  private _notes: Map<string, Note> = new Map()
  private _hitArea?: PIXI.Graphics

  constructor(width: number, height: number) {
    super({
      width: width,
      height: height,
      backgroundColor: 0xf7f3f2,
      resolution: window.devicePixelRatio || 1,
      autoDensity: true,
      antialias: true,
    })
    this.preload()
  }

  preload(): void {
    // load assets if needed
    this.loader
      .add('trash', '/sprites/trash.png')
      .load(() => this._onAssetsLoaded())
  }

  initStage(): void {
    this.renderer.plugins.interaction.cursorStyles.default = DEFAULT_CURSOR;
    this.renderer.plugins.interaction.cursorStyles.pointer = POINTER_CURSOR;
    this.renderer.plugins.interaction.cursorStyles.grabbing = DRAG_CURSOR;
    this.renderer.plugins.interaction.cursorStyles.grab = POINTER_DOWN_CURSOR;
    this._initHitArea()
    this._initTrash()
    this._apiManager = this._getApiManager()
    this._apiManager.connect()
  }

  private _getApiManager(): APIManager {

    const apiManager: APIManager = new APIManager()

    apiManager.onConnect = (payload: NoteDto[]) => {
      if (this._deleteAllNotes.length > 0) this._deleteAllNotes()
      payload.forEach(({ name, text, x, y }: NoteDto) => {
        this.createNote(x, y, text, name)
      })
    }

    apiManager.onNoteCreate = ({ x, y, name, text }: NoteDto) => {
      this.createNote(x,y,text,name)
    }

    apiManager.onNoteChange = (payload: NoteDto) => {
      console.log(payload);
      
      const note = this._notes.get(payload.name)
      if (note) note.update(payload)
    }

    apiManager.onNoteDelete = (name: string) => {
      const note = this._notes.get(name)
      if (note) this._deleteNote(note)
    }

    apiManager.onNoteLock = (name: string) => {
      this._notes.get(name)?.disable()
    }

    apiManager.onNoteUnlock = (name: string) => {
      this._notes.get(name)?.enable()
    }

    return apiManager
  }

  private _attachListeners(note: Note): void {
    const throttledChange = throttle(this._onChange, 200)
    const debounceChange = debounce(this._onChange, 200)
    note
      .on('dragstart', () => this._onDragStart(note))
      .on('dragmove', () => throttledChange(note))
      .on('input', () => debounceChange(note))
      .on('dragend', (e: PIXI.InteractionEvent) => this._onDragEnd(note, e))
      .on('blur', (e: PIXI.InteractionEvent) => this._apiManager?.pushNoteUnlock(note.name))
      .on('focus', () => this._apiManager?.pushNoteLock(note.name))
  }

  private _onDragStart(note: Note): void {
    this._apiManager?.pushNoteLock(note.name)
    if (!this._trash) return
    this._trash.appear()
  }

  private _onChange = (note: Note): void => {
    console.log('change');
    
    this._apiManager?.pushNoteChanges(note.toDto())
  }

  private _onDragEnd(note: Note, event: PIXI.InteractionEvent): void {
    if (!this._trash) return

    const bounds = this._trash.getBounds()
    const point = event.data.getLocalPosition(this.stage)
    if (bounds.contains(point.x, point.y)) {
      this._deleteNote(note)
      this._apiManager?.pushNoteDelete(note.name)
    }
    else {
      this._apiManager?.pushNoteChanges(note.toDto())
      this._apiManager?.pushNoteUnlock(note.name)
    }
    this._trash.dissapear()
  }

  private _deleteNote(note: Note): void {
    note.dissapear()
    if (this._notes.has(note.name)) {
      this._notes.delete(note.name)
    }
  }

  // creating a hit area for creating new notes
  private _initHitArea(): void {
    const size = this.renderer.screen
    this._hitArea = new PIXI.Graphics().beginFill(0xe5e0df).drawRect(0, 0, size.width, size.height).endFill()
    this._hitArea.interactive = true
    this._hitArea.on('pointertap', (e) => this._onPointerTap(e))
    this.stage.addChild(this._hitArea)
  }

  private _onPointerTap(event: PIXI.InteractionEvent): void {
    const localCoordinates = event.data.getLocalPosition(this.stage)
    const note = this.createNote(localCoordinates.x, localCoordinates.y)
    this._apiManager?.pushNoteCreate(note.toDto())
    note.focus()
  }

  // initializing trash bin for deleting the notes
  private _initTrash(): void {
    const texture = this.loader.resources['trash'].texture
    if (!texture) throw new Error('no trash bin texture')
    this._trash = new Trash(texture)
    const screenSize = this.renderer.screen
    this._trash.x = screenSize.width / 2 - this._trash.width / 2
    this._trash.y = screenSize.height - this._trash.height
    this.stage.addChild(this._trash)
  }

  // creating notes and focusing
  createNote(x: number, y: number, text: string = '', name: string = nanoid()): Note {
    const note = new Note(x, y, name, text)
    this._attachListeners(note)
    // storing our notes in the map
    this._notes.set(name, note)
    this.stage.addChild(note)
    return note
  }

  private _deleteAllNotes(): void {
    this._notes.forEach((note, name, map) => {
      note.dissapear()
      map.delete(name)
    })
  }

  private _onAssetsLoaded(): void {
    this.initStage()
  }

  onResize(width: number, height: number): void {
    this.renderer.resize(width, height)
    if (this._hitArea) {
      this._hitArea.height = height
      this._hitArea.width = width
    }
    if (this._trash) {
      this._trash.x = this.screen.width / 2 - this._trash.width / 2
      this._trash.y = this.screen.height - this._trash.height
    }
  }
}