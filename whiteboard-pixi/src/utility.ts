export function throttle(func: Function, limit: number): Function {
	let inThrottle: boolean;

	return function(this: any): any {
		const args = arguments;
		const context = this;

		if (!inThrottle) {
			inThrottle = true;
			func.apply(context, args);
			setTimeout(() => (inThrottle = false), limit);
		}
	};
}

export const debounce = <F extends (...args: any[]) => any>(func: F, waitFor: number) => {
  let timeout: ReturnType<typeof setTimeout> | null = null;

  const debounced = (...args: Parameters<F>) => {
    if (timeout !== null) {
      clearTimeout(timeout);
      timeout = null;
    }
    timeout = setTimeout(() => func(...args), waitFor);
  };

  return debounced as (...args: Parameters<F>) => ReturnType<F>;
};