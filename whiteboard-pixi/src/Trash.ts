import anime from 'animejs'
import * as PIXI from 'pixi.js'

const DEFAULT_TRASH_SIZE = 300
const DEFAULT_ROUNDING = 24

export class Trash extends PIXI.Container {
  
  private _trashIcon: PIXI.Sprite
  private _outline: PIXI.Graphics

  constructor(texture: PIXI.Texture) {
    super()
    this._outline = new PIXI.Graphics().lineStyle({ width: 4, color: 0x000000 }).beginFill(0xfa4d56, 0.4).drawRoundedRect(0,0,DEFAULT_TRASH_SIZE, DEFAULT_TRASH_SIZE / 2, DEFAULT_ROUNDING).endFill()
    this._trashIcon = new PIXI.Sprite(texture)
    this._trashIcon.width = 32
    this._trashIcon.height = 32
    this._trashIcon.anchor.set(.5, .5)
    this._trashIcon.x = this._outline.width / 2
    this._trashIcon.y = this._outline.height / 2
    this.addChild(this._outline, this._trashIcon)
    this.interactive = true
    this.alpha = 0
  }

  appear() {
    anime({
      targets: this,
      alpha: 1,
      easing: 'easeOutCirc',
      duration: 200
    })
  }

  dissapear() {
    anime({
      targets: this,
      alpha: 0,
      easing: 'easeOutCirc',
      duration: 200
    })
  }
}