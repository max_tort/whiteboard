import randomcolor from 'randomcolor'

export class User {
  id: string
  username: string
  color: string

  constructor(id: string, username: string, color: string = randomcolor()) { 
    this.id = id
    this.username = username
    this.color = color
  }
}