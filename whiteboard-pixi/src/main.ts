import { Application } from "./App"
import { debounce } from "./utility"

const app: HTMLElement = document.getElementById('app')!

const myApp = new Application(window.innerWidth, window.innerHeight)

const debouncedOnResize = debounce(myApp.onResize.bind(myApp), 100)

window.onresize = () => debouncedOnResize(window.innerWidth, window.innerHeight)

app.appendChild(myApp.view)