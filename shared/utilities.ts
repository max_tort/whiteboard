
export const enum GatewayEvents {
  onNoteChange = 'notechange',
  onNoteDelete = 'notedelete',
  onNoteCreate = 'notecreate',
  onNoteLock = 'notelock',
  onNoteUnlock = 'noteunlock'
}