
export interface NoteDto {
  name: string
  text: string
  x: number
  y: number
}